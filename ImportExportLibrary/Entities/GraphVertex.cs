﻿namespace ImportExportLibrary.Entities
{
    using System.Collections.Generic;

    /// <summary>
    /// Вершина графа.
    /// </summary>
    public class GraphVertex
    {
        /// <summary>
        /// Метка.
        /// </summary>
        public string Label { get; set; }

        /// <summary>
        /// Список исходящих ребер.
        /// </summary>
        public Dictionary<string, GraphEdge> Edges { get; set; }

        /// <summary>
        /// Список входящих ребер.
        /// </summary>
        public Dictionary<string, GraphEdge> InputEdges { get; set; }

        public GraphVertex(string label)
        {
            Label = label;
            Edges = new Dictionary<string, GraphEdge>();
            InputEdges = new Dictionary<string, GraphEdge>();
        }
    }
}
