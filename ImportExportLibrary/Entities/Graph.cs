﻿namespace ImportExportLibrary.Entities
{
    using System.Collections.Generic;
    using System.Linq;
    using System;

    public enum ImportedFrom
    {
        AdjacencyMatrix,
        IncidenceMatrix,
        AdjacencyList,
        EdgeList
    }

    /// <summary>
    /// Граф.
    /// </summary>
    public class Graph
    {
        private int _maxValue = 10000;

        private List<string> _used;

        private List<string> _comp;

        /// <summary>
        /// Ориентированный ли граф.
        /// </summary>
        public bool IsOriented { get; set; }

        /// <summary>
        /// Вершины.
        /// </summary>
        public List<GraphVertex> Vertices { get; set; }

        /// <summary>
        /// Ребра.
        /// </summary>
        public List<GraphEdge> Edges { get; set; }

        /// <summary>
        /// Количество вершин в графе.
        /// </summary>
        public int VerticesCount => Vertices.Count;

        /// <summary>
        /// Импортировано из.
        /// </summary>
        public ImportedFrom ImportedFrom { get; set; }

        /// <summary>
        /// Степень графа (максимальная степень вершин).
        /// </summary>
        public int Degree => 
            IsOriented 
            ? 
            Vertices.Max(vert => vert.Edges.Count + vert.InputEdges.Count)
            : 
            Vertices.Max(vert => vert.Edges.Count);

        /// <summary>
        /// Количество ребер в графе.
        /// </summary>
        public int EdgesCount => IsOriented ? Edges.Count : Edges.Count / 2;

        /// <summary>
        /// Матрица смежности графа.
        /// </summary>
        public int[,] AdjacencyMatrix => CreateAdjacencyMatrix();

        /// <summary>
        /// Диаметр.
        /// </summary>
        public int Diameter
        {
            get
            {
                var arrayExc = CreateExeccentricity();
                //если есть расстояния ток из бесконечностей то я не могу назвать диаметр графа
                return arrayExc.Count(x => x == 0) != 0 ? int.MaxValue : arrayExc.Max();
            }
        }

        /// <summary>
        /// Радиус.
        /// </summary>
        public int Radius
        {
            get
            {
                var nonZeroExc = CreateExeccentricity().Where(x => x != 0);
                return nonZeroExc.Count() > 0 ? nonZeroExc.Min() : 0;
            }
        } 

        /// <summary>
        /// Число компонент связности.
        /// </summary>
        public int ComponentsCount => CalculateComponentsCount();

        /// <summary>
        /// Цикломатическое число.
        /// </summary>
        public int CyclomaticNumber => EdgesCount - VerticesCount + ComponentsCount;

        /// <summary>
        /// Является ли граф деревом.
        /// </summary>
        public bool IsTree => ComponentsCount == 1 && EdgesCount == VerticesCount - 1;

        public Graph()
        {
            Edges = new List<GraphEdge>();
            Vertices = new List<GraphVertex>();
        }

        /// <summary>
        /// Ищет ребро и его вес.
        /// </summary>
        /// <param name="from">Метка исходящей вершины.</param>
        /// <param name="to">Метка конечной вершины.</param>
        /// <returns>Вес ребра или -1, если такого ребра не существует.</returns>
        public int FindEdgeWeight(string from, string to)
        {
            return Edges.FirstOrDefault(e => e.From == from && e.To == to)?.Weight ?? -1;
        }

        /// <summary>
        /// Ищет вершину по метке.
        /// </summary>
        /// <param name="label">Метка.</param>
        /// <returns>Вершина.</returns>
        public GraphVertex FindVertex(string label)
        {
            return Vertices.FirstOrDefault(v => v.Label == label);
        }

        /// <summary>
        /// Выводит смежные вершины.
        /// </summary>
        /// <param name="label">Метка вершины.</param>
        /// <returns>Список смежных вершин.</returns>
        public List<GraphVertex> FindAdjacentVertices(string label)
        {
            if (label == null)
            {
                return null;
            }

            var vertex = FindVertex(label);
            var resultList = new List<GraphVertex>();
            foreach (var edge in vertex.Edges)
            {
                if (!resultList.Exists(v => v.Label == edge.Key))
                {
                    resultList.Add(FindVertex(edge.Key));
                }
            }
            if (IsOriented)
            {
                foreach (var edge in vertex.InputEdges)
                {
                    if (!resultList.Exists(v => v.Label == edge.Key))
                    {
                        resultList.Add(FindVertex(edge.Key));
                    }
                }
            }

            return resultList.Where(v => v != null).ToList();
        }

        /// <summary>
        /// Удаление вершины.
        /// </summary>
        /// <param name="label">Метка вершины.</param>
        public void DeleteVertex(string label)
        {
            Vertices.Remove(Vertices.FirstOrDefault(x => x.Label == label));
            Edges.RemoveAll(e => e.From == label || e.To == label);
        }

        /// <summary>
        /// Удаление ребра.
        /// </summary>
        /// <param name="from">Исходящая вершина.</param>
        /// <param name="to">Входящая вершина.</param>
        public void DeleteEdge(string from, string to)
        {
            Edges.RemoveAll(e => e.From == from && e.To == to || e.To == from && e.From == to);
            var fromV = FindVertex(from);
            var toV = FindVertex(to);
            fromV.Edges.Remove(to);
            toV.InputEdges.Remove(from);
        }

        /// <summary>
        /// Добавить вершину.
        /// </summary>
        /// <param name="label">Метка новой вершины.</param>
        public void AddVertex(string label)
        {
            Vertices.Add(new GraphVertex(label));
        }

        /// <summary>
        /// Добавить ребро.
        /// </summary>
        /// <param name="from">Исходящая вершина.</param>
        /// <param name="to">Конечная вершина.</param>
        /// <param name="weight">Вес.</param>
        public void AddEdge(string from, string to, int weight)
        {
            var newEdge = new GraphEdge(from, to, weight);
            var invertedNewEdge = new GraphEdge(to, from, weight);
            Edges.Add(newEdge);
            var fromVertex = FindVertex(from);
            var toVertex = FindVertex(to);
            fromVertex.Edges.Add(to, newEdge);
            toVertex.InputEdges.Add(from, newEdge);
            if (!IsOriented)
            {
                Edges.Add(invertedNewEdge);
                toVertex.Edges.Add(from, invertedNewEdge);
                fromVertex.InputEdges.Add(to, invertedNewEdge);
            }
        }

        private int[,] CreateAdjacencyMatrix()
        {
            int[,] resultMatrix = new int[VerticesCount, VerticesCount];
            for (int i = 0; i < VerticesCount; i++)
            {
                for (int j = 0; j < VerticesCount; j++)
                {
                    resultMatrix[i, j] = FindEdgeWeight(Vertices.ElementAt(i).Label, Vertices.ElementAt(j).Label);
                }
            }

            return resultMatrix;
        }

        private int[] CreateExeccentricity()
        {
            int[,] matrixLength = (int[,])AdjacencyMatrix.Clone();
            for (var i = 0; i < VerticesCount; i++)
            {
                for (var j = 0; j < VerticesCount; j++)
                {
                    matrixLength[i, j] = matrixLength[i, j] == -1 ? _maxValue : matrixLength[i, j];
                }
            }

            //считаю расстояния
            for (var i = 0; i < VerticesCount; i++)
            {
                for (var j = 0; j < VerticesCount; j++)
                {
                    for (var k = 0; k < VerticesCount; k++)
                    {
                        matrixLength[i, j] = (int)Math.Min((double)matrixLength[i, j], matrixLength[i, k] + matrixLength[k, j]);
                    }
                }
            }

            int[] arrayExc = new int[VerticesCount];
            for (int i = 0; i < VerticesCount; i++)
            {
                for (int j = 0; j < VerticesCount; j++)
                {
                    //если расстояние есть
                    if (i != j && matrixLength[i, j] != _maxValue)
                    {
                        //ищу расстояние максимальное среди расстояний от вершины i до остальных
                        arrayExc[i] = Math.Max(matrixLength[i, j], arrayExc[i]);
                    }
                }
            }
            return arrayExc;
        }

        public bool IsEdgeExists(string from, string to)
        {
            return Edges.Exists(e => e.From == from && e.To == to);
        }

        private void DFS(string v)
        {
            _used.Add(v);
            _comp.Add(v);
            foreach (var edge in FindVertex(v).Edges)
            {
                if (!_used.Contains(edge.Key))
                {
                    DFS(edge.Key);
                }
            }
        }

        private int CalculateComponentsCount()
        {
            _used = new List<string>();
            _comp = new List<string>();
            int countComponents = 0;
            foreach (var vertex in Vertices)
            {
                if (!_used.Contains(vertex.Label))
                {
                    _comp.Clear();
                    DFS(vertex.Label);
                    countComponents++;
                }
            }
            return countComponents;
        }
    }
}
