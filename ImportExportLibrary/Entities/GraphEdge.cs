﻿namespace ImportExportLibrary.Entities
{
    /// <summary>
    /// Ребро графа.
    /// </summary>
    public class GraphEdge
    {
        /// <summary>
        /// Откуда.
        /// </summary>
        public string From { get; set; }

        /// <summary>
        /// Куда.
        /// </summary>
        public string To { get; set; }

        /// <summary>
        /// Вес ребра.
        /// </summary>
        public int Weight { get; set; }

        public GraphEdge(string from, string to, int weight = -1)
        {
            From = from;
            To = to;
            Weight = weight;
        }

        public GraphEdge()
        {
            
        }
    }
}
