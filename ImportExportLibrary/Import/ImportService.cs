﻿namespace ImportExportLibrary.Import
{
    using Entities;
    using System;
    using System.IO;
    using System.Linq;
    using System.Collections.Generic;

    /// <summary>
    /// Сервис импорта графа из файла.
    /// </summary>
    public class ImportService
    {
        public StreamReader File { get; set; }

        private Graph Graph { get; set; }

        public ImportService(string fileName, bool isOriented = false)
        {
            File = new StreamReader(fileName);
            Graph = new Graph();
            Graph.IsOriented = isOriented;
        }

        /// <summary>
        /// Импорт из матрицы смежности.
        /// </summary>
        /// <returns>Граф.</returns>
        public Graph FromAdjacencyMatrix()
        {
            var elements = File.ReadLine()?.Split(' ');
            int n = elements.Count();
            int j = 0;
            Graph.Vertices = CreateEmptyVertices(n);
            while (j<n)
            {
                int weight;
                for (int i = 0; i < n; i++)
                {
                    weight = int.Parse(elements[i]);
                    if (weight != -1)
                    {
                        var newEdge = new GraphEdge(j.ToString(), i.ToString(), weight);
                        Graph.Edges.Add(newEdge);
                        var from = Graph.FindVertex(j.ToString());
                        var to = Graph.FindVertex(i.ToString());
                        from.Edges.Add(i.ToString(), newEdge);
                        to.InputEdges.Add(j.ToString(), newEdge);
                    }
                }
                elements = File.ReadLine()?.Split(' ');
                j++;
            }
            CloseFile();
            Graph.ImportedFrom = ImportedFrom.AdjacencyMatrix;
            return Graph;
        }

        /// <summary>
        /// Импорт из матрицы инцидентности.
        /// </summary>
        /// <returns>Граф.</returns>
        public Graph FromIncidenceMatrix()
        {
            var elements = File.ReadLine()?.Split(' ');
            int n = elements.Length, j = 0;
            GraphEdge[] edges = new GraphEdge[n];
            edges = CreateEmptyEdges(n);
            for (int i = 0; i < n; i++)
            {
                edges[i].Weight = Int32.Parse(elements[i]);
            }

            while (!File.EndOfStream)
            {
                elements = File.ReadLine()?.Split(' ');
                var newVertex = new GraphVertex(j.ToString());
                for (int i = 0; i < n; i++)
                {
                    int sign = int.Parse(elements[i]);
                    if (sign != 0)
                    {
                        if (sign == 1)
                        {
                            edges[i].From = j.ToString();
                        }
                        else
                        {
                            edges[i].To = j.ToString();
                        }
                    }
                }
                Graph.Vertices.Add(newVertex);
                j++;
            }
            foreach (var edge in edges)
            {
                var fromVertex = Graph.FindVertex(edge.From);
                fromVertex.Edges.Add(edge.To, edge);
                var toVertex = Graph.FindVertex(edge.To);
                toVertex.InputEdges.Add(edge.From, edge);
            }

            Graph.Edges = edges.ToList();

            CloseFile();
            Graph.ImportedFrom = ImportedFrom.IncidenceMatrix;

            return Graph;
        }

        /// <summary>
        /// Импорт из списка смежности.
        /// </summary>
        /// <returns>Граф.</returns>
        public Graph FromAdjacencyList()
        {
            var elements = File.ReadLine()?.Split(' ');
            int n = elements.Length;
            var vertices = CreateEmptyVertices(n);
            
            int i = 0;
            foreach (var vertice in vertices)
            {
                vertice.Label = elements[i];
                i++;
            }

            Graph.Vertices = vertices;

            while (!File.EndOfStream)
            {
                elements = File.ReadLine()?.Split(' ');
                n = elements.Length;
                var from = Graph.FindVertex(elements[0]);
                int j = 1;
                while (j < n)
                {
                    var newEdge = new GraphEdge(elements[0], elements[j], int.Parse(elements[j+1]));
                    from.Edges.Add(newEdge.To, newEdge);
                    var to = Graph.FindVertex(elements[j]);
                    to.InputEdges.Add(newEdge.From, newEdge);
                    Graph.Edges.Add(newEdge);
                    j+=2;
                }
            }
            CloseFile();
            Graph.ImportedFrom = ImportedFrom.AdjacencyList;

            return Graph;
        }

        /// <summary>
        /// Импорт из списка ребер.
        /// </summary>
        /// <returns>Граф.</returns>
        public Graph FromEdgeList()
        {
            while (!File.EndOfStream)
            {
                var elements = File.ReadLine()?.Split(' ');
                GraphEdge newEdge = new GraphEdge(elements[0], elements[1], int.Parse(elements[2]));
                Graph.Edges.Add(newEdge);
                GraphVertex from = Graph.FindVertex(elements[0]);
                if (from == null)
                {
                    from = new GraphVertex(elements[0]);
                    Graph.Vertices.Add(from);
                }
                GraphVertex to = Graph.FindVertex(elements[1]);
                if (to == null)
                {
                    to = new GraphVertex(elements[1]);
                    Graph.Vertices.Add(to);
                }
                from.Edges.Add(newEdge.To, newEdge);
                to.InputEdges.Add(newEdge.From, newEdge);
            }

            CloseFile();
            Graph.ImportedFrom = ImportedFrom.EdgeList;
            return Graph;
        }

        public void CloseFile()
        {
            File.Close();
        }

        private List<GraphVertex> CreateEmptyVertices(int n)
        {
            List<GraphVertex> result = new List<GraphVertex>();
            for (int i = 0; i < n; i++)
            {
                result.Add(new GraphVertex(i.ToString()));
            }
            return result;
        }

        private GraphEdge[] CreateEmptyEdges(int n)
        {
            GraphEdge[] result = new GraphEdge[n];
            for (int i = 0; i < n; i++)
            {
                result[i] = new GraphEdge();
            }
            return result;
        }
    }
}
