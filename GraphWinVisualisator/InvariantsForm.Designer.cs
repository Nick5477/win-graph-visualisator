﻿namespace GraphWinVisualisator
{
    partial class InvariantsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lblVertexCount = new System.Windows.Forms.Label();
            this.lblEdgeCount = new System.Windows.Forms.Label();
            this.lblGraphDegree = new System.Windows.Forms.Label();
            this.lblDiametr = new System.Windows.Forms.Label();
            this.lblRadius = new System.Windows.Forms.Label();
            this.lblCycleNumber = new System.Windows.Forms.Label();
            this.lblCountComponent = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lblIsTree = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Инварианты:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(110, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Количество вершин:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(14, 51);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(102, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Количество ребер:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 76);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(86, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Степень графа:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(14, 101);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Диаметр:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 126);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(46, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Радиус:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 152);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(109, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Циклическое число:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(12, 175);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(156, 13);
            this.label9.TabIndex = 8;
            this.label9.Text = "Число компонент связности:";
            // 
            // lblVertexCount
            // 
            this.lblVertexCount.AutoSize = true;
            this.lblVertexCount.Location = new System.Drawing.Point(132, 30);
            this.lblVertexCount.Name = "lblVertexCount";
            this.lblVertexCount.Size = new System.Drawing.Size(41, 13);
            this.lblVertexCount.TabIndex = 9;
            this.lblVertexCount.Text = "label10";
            // 
            // lblEdgeCount
            // 
            this.lblEdgeCount.AutoSize = true;
            this.lblEdgeCount.Location = new System.Drawing.Point(132, 51);
            this.lblEdgeCount.Name = "lblEdgeCount";
            this.lblEdgeCount.Size = new System.Drawing.Size(41, 13);
            this.lblEdgeCount.TabIndex = 10;
            this.lblEdgeCount.Text = "label11";
            // 
            // lblGraphDegree
            // 
            this.lblGraphDegree.AutoSize = true;
            this.lblGraphDegree.Location = new System.Drawing.Point(132, 76);
            this.lblGraphDegree.Name = "lblGraphDegree";
            this.lblGraphDegree.Size = new System.Drawing.Size(41, 13);
            this.lblGraphDegree.TabIndex = 11;
            this.lblGraphDegree.Text = "label12";
            // 
            // lblDiametr
            // 
            this.lblDiametr.AutoSize = true;
            this.lblDiametr.Location = new System.Drawing.Point(132, 101);
            this.lblDiametr.Name = "lblDiametr";
            this.lblDiametr.Size = new System.Drawing.Size(41, 13);
            this.lblDiametr.TabIndex = 12;
            this.lblDiametr.Text = "label13";
            // 
            // lblRadius
            // 
            this.lblRadius.AutoSize = true;
            this.lblRadius.Location = new System.Drawing.Point(132, 126);
            this.lblRadius.Name = "lblRadius";
            this.lblRadius.Size = new System.Drawing.Size(41, 13);
            this.lblRadius.TabIndex = 13;
            this.lblRadius.Text = "label14";
            // 
            // lblCycleNumber
            // 
            this.lblCycleNumber.AutoSize = true;
            this.lblCycleNumber.Location = new System.Drawing.Point(132, 152);
            this.lblCycleNumber.Name = "lblCycleNumber";
            this.lblCycleNumber.Size = new System.Drawing.Size(41, 13);
            this.lblCycleNumber.TabIndex = 14;
            this.lblCycleNumber.Text = "label15";
            // 
            // lblCountComponent
            // 
            this.lblCountComponent.AutoSize = true;
            this.lblCountComponent.Location = new System.Drawing.Point(174, 175);
            this.lblCountComponent.Name = "lblCountComponent";
            this.lblCountComponent.Size = new System.Drawing.Size(41, 13);
            this.lblCountComponent.TabIndex = 16;
            this.lblCountComponent.Text = "label17";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 197);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(121, 13);
            this.label8.TabIndex = 17;
            this.label8.Text = "Является ли деревом:";
            // 
            // lblIsTree
            // 
            this.lblIsTree.AutoSize = true;
            this.lblIsTree.Location = new System.Drawing.Point(148, 197);
            this.lblIsTree.Name = "lblIsTree";
            this.lblIsTree.Size = new System.Drawing.Size(41, 13);
            this.lblIsTree.TabIndex = 18;
            this.lblIsTree.Text = "label17";
            // 
            // InvariantsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.lblIsTree);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.lblCountComponent);
            this.Controls.Add(this.lblCycleNumber);
            this.Controls.Add(this.lblRadius);
            this.Controls.Add(this.lblDiametr);
            this.Controls.Add(this.lblGraphDegree);
            this.Controls.Add(this.lblEdgeCount);
            this.Controls.Add(this.lblVertexCount);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "InvariantsForm";
            this.Text = "InvariantsForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lblVertexCount;
        private System.Windows.Forms.Label lblEdgeCount;
        private System.Windows.Forms.Label lblGraphDegree;
        private System.Windows.Forms.Label lblDiametr;
        private System.Windows.Forms.Label lblRadius;
        private System.Windows.Forms.Label lblCycleNumber;
        private System.Windows.Forms.Label lblCountComponent;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblIsTree;
    }
}