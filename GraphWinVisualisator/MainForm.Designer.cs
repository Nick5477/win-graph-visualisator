﻿namespace GraphWinVisualisator
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvEdges = new System.Windows.Forms.DataGridView();
            this.A = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.B = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Weight = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dgvVertices = new System.Windows.Forms.DataGridView();
            this.Label = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.данныеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.импортToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.матрицаСмежностиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.матрицаИнцидентностиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.списокСмежностиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.списокРеберToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.экспортToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.инвариантыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label3 = new System.Windows.Forms.Label();
            this.lblFrom = new System.Windows.Forms.Label();
            this.tbVertexSearch = new System.Windows.Forms.TextBox();
            this.btnVertexSearch = new System.Windows.Forms.Button();
            this.cbFrom = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cbTo = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblGraphType = new System.Windows.Forms.Label();
            this.dgvAdjacencyVertices = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label7 = new System.Windows.Forms.Label();
            this.btnSearchGraphEdge = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btnDraw = new System.Windows.Forms.Button();
            this.pBoxGraph = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEdges)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVertices)).BeginInit();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAdjacencyVertices)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pBoxGraph)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvEdges
            // 
            this.dgvEdges.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvEdges.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.A,
            this.B,
            this.Weight});
            this.dgvEdges.Location = new System.Drawing.Point(25, 51);
            this.dgvEdges.Name = "dgvEdges";
            this.dgvEdges.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvEdges.Size = new System.Drawing.Size(343, 119);
            this.dgvEdges.TabIndex = 0;
            this.dgvEdges.RowValidated += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvEdges_RowValidated);
            this.dgvEdges.RowValidating += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dgvEdges_RowValidating);
            this.dgvEdges.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.dgvEdges_UserDeletingRow);
            // 
            // A
            // 
            this.A.HeaderText = "Вершина 1";
            this.A.Name = "A";
            this.A.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.A.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // B
            // 
            this.B.HeaderText = "Вершина 2";
            this.B.Name = "B";
            this.B.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.B.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // Weight
            // 
            this.Weight.HeaderText = "Вес ребра";
            this.Weight.Name = "Weight";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Ребра";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 173);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Вершины";
            // 
            // dgvVertices
            // 
            this.dgvVertices.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvVertices.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Label});
            this.dgvVertices.Location = new System.Drawing.Point(23, 219);
            this.dgvVertices.Name = "dgvVertices";
            this.dgvVertices.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvVertices.Size = new System.Drawing.Size(144, 132);
            this.dgvVertices.TabIndex = 3;
            this.dgvVertices.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvVertices_RowEnter);
            this.dgvVertices.RowValidating += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dgvVertices_RowValidating);
            this.dgvVertices.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.dgvVertices_UserDeletingRow);
            // 
            // Label
            // 
            this.Label.HeaderText = "Метка";
            this.Label.Name = "Label";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.данныеToolStripMenuItem,
            this.инвариантыToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(883, 24);
            this.menuStrip1.TabIndex = 4;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // данныеToolStripMenuItem
            // 
            this.данныеToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.импортToolStripMenuItem,
            this.экспортToolStripMenuItem});
            this.данныеToolStripMenuItem.Name = "данныеToolStripMenuItem";
            this.данныеToolStripMenuItem.Size = new System.Drawing.Size(62, 20);
            this.данныеToolStripMenuItem.Text = "Данные";
            // 
            // импортToolStripMenuItem
            // 
            this.импортToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.матрицаСмежностиToolStripMenuItem,
            this.матрицаИнцидентностиToolStripMenuItem,
            this.списокСмежностиToolStripMenuItem,
            this.списокРеберToolStripMenuItem});
            this.импортToolStripMenuItem.Name = "импортToolStripMenuItem";
            this.импортToolStripMenuItem.Size = new System.Drawing.Size(119, 22);
            this.импортToolStripMenuItem.Text = "Импорт";
            // 
            // матрицаСмежностиToolStripMenuItem
            // 
            this.матрицаСмежностиToolStripMenuItem.Name = "матрицаСмежностиToolStripMenuItem";
            this.матрицаСмежностиToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.матрицаСмежностиToolStripMenuItem.Text = "Матрица смежности";
            this.матрицаСмежностиToolStripMenuItem.Click += new System.EventHandler(this.матрицаСмежностиToolStripMenuItem_Click);
            // 
            // матрицаИнцидентностиToolStripMenuItem
            // 
            this.матрицаИнцидентностиToolStripMenuItem.Name = "матрицаИнцидентностиToolStripMenuItem";
            this.матрицаИнцидентностиToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.матрицаИнцидентностиToolStripMenuItem.Text = "Матрица инцидентности";
            this.матрицаИнцидентностиToolStripMenuItem.Click += new System.EventHandler(this.матрицаИнцидентностиToolStripMenuItem_Click);
            // 
            // списокСмежностиToolStripMenuItem
            // 
            this.списокСмежностиToolStripMenuItem.Name = "списокСмежностиToolStripMenuItem";
            this.списокСмежностиToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.списокСмежностиToolStripMenuItem.Text = "Список смежности";
            this.списокСмежностиToolStripMenuItem.Click += new System.EventHandler(this.списокСмежностиToolStripMenuItem_Click);
            // 
            // списокРеберToolStripMenuItem
            // 
            this.списокРеберToolStripMenuItem.Name = "списокРеберToolStripMenuItem";
            this.списокРеберToolStripMenuItem.Size = new System.Drawing.Size(210, 22);
            this.списокРеберToolStripMenuItem.Text = "Список ребер";
            this.списокРеберToolStripMenuItem.Click += new System.EventHandler(this.списокРеберToolStripMenuItem_Click);
            // 
            // экспортToolStripMenuItem
            // 
            this.экспортToolStripMenuItem.Name = "экспортToolStripMenuItem";
            this.экспортToolStripMenuItem.Size = new System.Drawing.Size(119, 22);
            this.экспортToolStripMenuItem.Text = "Экспорт";
            // 
            // инвариантыToolStripMenuItem
            // 
            this.инвариантыToolStripMenuItem.Name = "инвариантыToolStripMenuItem";
            this.инвариантыToolStripMenuItem.Size = new System.Drawing.Size(88, 20);
            this.инвариантыToolStripMenuItem.Text = "Инварианты";
            this.инвариантыToolStripMenuItem.Click += new System.EventHandler(this.инвариантыToolStripMenuItem_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(374, 41);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(103, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Импортировано из";
            // 
            // lblFrom
            // 
            this.lblFrom.AutoSize = true;
            this.lblFrom.Location = new System.Drawing.Point(483, 41);
            this.lblFrom.Name = "lblFrom";
            this.lblFrom.Size = new System.Drawing.Size(0, 13);
            this.lblFrom.TabIndex = 7;
            // 
            // tbVertexSearch
            // 
            this.tbVertexSearch.Location = new System.Drawing.Point(23, 193);
            this.tbVertexSearch.Name = "tbVertexSearch";
            this.tbVertexSearch.Size = new System.Drawing.Size(100, 20);
            this.tbVertexSearch.TabIndex = 8;
            // 
            // btnVertexSearch
            // 
            this.btnVertexSearch.Location = new System.Drawing.Point(125, 192);
            this.btnVertexSearch.Name = "btnVertexSearch";
            this.btnVertexSearch.Size = new System.Drawing.Size(75, 23);
            this.btnVertexSearch.TabIndex = 9;
            this.btnVertexSearch.Text = "Поиск";
            this.btnVertexSearch.UseVisualStyleBackColor = true;
            this.btnVertexSearch.Click += new System.EventHandler(this.btnVertexSearch_Click);
            // 
            // cbFrom
            // 
            this.cbFrom.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbFrom.FormattingEnabled = true;
            this.cbFrom.Location = new System.Drawing.Point(73, 15);
            this.cbFrom.Name = "cbFrom";
            this.cbFrom.Size = new System.Drawing.Size(94, 21);
            this.cbFrom.TabIndex = 10;
            this.cbFrom.SelectedIndexChanged += new System.EventHandler(this.cbFrom_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(24, 18);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Откуда";
            // 
            // cbTo
            // 
            this.cbTo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTo.FormattingEnabled = true;
            this.cbTo.Location = new System.Drawing.Point(207, 15);
            this.cbTo.Name = "cbTo";
            this.cbTo.Size = new System.Drawing.Size(81, 21);
            this.cbTo.TabIndex = 12;
            this.cbTo.SelectedIndexChanged += new System.EventHandler(this.cbTo_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(170, 18);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(31, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "Куда";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(374, 65);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(63, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "Тип графа:";
            // 
            // lblGraphType
            // 
            this.lblGraphType.AutoSize = true;
            this.lblGraphType.Location = new System.Drawing.Point(443, 65);
            this.lblGraphType.Name = "lblGraphType";
            this.lblGraphType.Size = new System.Drawing.Size(0, 13);
            this.lblGraphType.TabIndex = 15;
            // 
            // dgvAdjacencyVertices
            // 
            this.dgvAdjacencyVertices.AllowUserToAddRows = false;
            this.dgvAdjacencyVertices.AllowUserToDeleteRows = false;
            this.dgvAdjacencyVertices.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAdjacencyVertices.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1});
            this.dgvAdjacencyVertices.Location = new System.Drawing.Point(224, 219);
            this.dgvAdjacencyVertices.Name = "dgvAdjacencyVertices";
            this.dgvAdjacencyVertices.ReadOnly = true;
            this.dgvAdjacencyVertices.Size = new System.Drawing.Size(144, 132);
            this.dgvAdjacencyVertices.TabIndex = 16;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "Метка";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(231, 203);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(105, 13);
            this.label7.TabIndex = 17;
            this.label7.Text = "Смежные вершины";
            // 
            // btnSearchGraphEdge
            // 
            this.btnSearchGraphEdge.Location = new System.Drawing.Point(294, 15);
            this.btnSearchGraphEdge.Name = "btnSearchGraphEdge";
            this.btnSearchGraphEdge.Size = new System.Drawing.Size(75, 23);
            this.btnSearchGraphEdge.TabIndex = 18;
            this.btnSearchGraphEdge.Text = "Поиск";
            this.btnSearchGraphEdge.UseVisualStyleBackColor = true;
            this.btnSearchGraphEdge.Click += new System.EventHandler(this.btnSearchGraphEdge_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(12, 27);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(871, 388);
            this.tabControl1.TabIndex = 19;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.lblFrom);
            this.tabPage1.Controls.Add(this.lblGraphType);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.btnSearchGraphEdge);
            this.tabPage1.Controls.Add(this.dgvEdges);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.dgvAdjacencyVertices);
            this.tabPage1.Controls.Add(this.cbTo);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.dgvVertices);
            this.tabPage1.Controls.Add(this.cbFrom);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.tbVertexSearch);
            this.tabPage1.Controls.Add(this.btnVertexSearch);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(863, 362);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Основное";
            // 
            // tabPage2
            // 
            this.tabPage2.AutoScroll = true;
            this.tabPage2.Controls.Add(this.btnDraw);
            this.tabPage2.Controls.Add(this.pBoxGraph);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(863, 362);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Рисунок";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // btnDraw
            // 
            this.btnDraw.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDraw.Location = new System.Drawing.Point(751, 31);
            this.btnDraw.Name = "btnDraw";
            this.btnDraw.Size = new System.Drawing.Size(89, 23);
            this.btnDraw.TabIndex = 1;
            this.btnDraw.Text = "Нарисовать!";
            this.btnDraw.UseVisualStyleBackColor = true;
            this.btnDraw.Click += new System.EventHandler(this.btnDraw_Click);
            // 
            // pBoxGraph
            // 
            this.pBoxGraph.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.pBoxGraph.Location = new System.Drawing.Point(20, 3);
            this.pBoxGraph.Name = "pBoxGraph";
            this.pBoxGraph.Size = new System.Drawing.Size(705, 349);
            this.pBoxGraph.TabIndex = 0;
            this.pBoxGraph.TabStop = false;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(883, 418);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.tabControl1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "GraphVisualisator";
            ((System.ComponentModel.ISupportInitialize)(this.dgvEdges)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVertices)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAdjacencyVertices)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pBoxGraph)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvEdges;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView dgvVertices;
        private System.Windows.Forms.DataGridViewTextBoxColumn Label;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem данныеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem импортToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem экспортToolStripMenuItem;
        private System.Windows.Forms.DataGridViewComboBoxColumn A;
        private System.Windows.Forms.DataGridViewComboBoxColumn B;
        private System.Windows.Forms.DataGridViewTextBoxColumn Weight;
        private System.Windows.Forms.ToolStripMenuItem инвариантыToolStripMenuItem;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblFrom;
        private System.Windows.Forms.TextBox tbVertexSearch;
        private System.Windows.Forms.Button btnVertexSearch;
        private System.Windows.Forms.ComboBox cbFrom;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbTo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblGraphType;
        private System.Windows.Forms.DataGridView dgvAdjacencyVertices;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnSearchGraphEdge;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.PictureBox pBoxGraph;
        private System.Windows.Forms.Button btnDraw;
        private System.Windows.Forms.ToolStripMenuItem матрицаСмежностиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem матрицаИнцидентностиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem списокСмежностиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem списокРеберToolStripMenuItem;
    }
}

