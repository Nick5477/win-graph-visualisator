﻿namespace GraphWinVisualisator
{
    using System.Windows.Forms;
    using ImportExportLibrary.Entities;

    public partial class InvariantsForm : Form
    {
        public Graph Graph { get; }

        public InvariantsForm()
        {
            InitializeComponent();
        }

        public InvariantsForm(Graph graph)
        {
            Graph = graph;
            InitializeComponent();
            InitializeInvariants();
        }

        private void InitializeInvariants()
        {
            lblVertexCount.Text = Graph.VerticesCount.ToString();
            lblEdgeCount.Text = Graph.EdgesCount.ToString();
            lblGraphDegree.Text = Graph.Degree.ToString();
            lblDiametr.Text = Graph.Diameter.ToString();
            lblRadius.Text = Graph.Radius.ToString();
            lblCountComponent.Text = Graph.ComponentsCount.ToString();
            lblCycleNumber.Text = Graph.CyclomaticNumber.ToString();
            lblIsTree.Text = Graph.IsTree ? "да" : "нет";
        }
    }
}
