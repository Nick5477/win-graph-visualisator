﻿namespace GraphWinVisualisator
{
    using QuickGraph;
    using QuickGraph.Graphviz;
    using QuickGraph.Graphviz.Dot;
    using System.Drawing;
    using System.IO;
    using ImportExportLibrary.Entities;

    public static class GraphDrawer
    {
        public static void DrawGraph(Graph graph)
        {
            var g = new AdjacencyGraph<string, TaggedEdge<string, string>>();
            //Добавялю вершины
            foreach (var vertex in graph.Vertices)
            {
                g.AddVertex(vertex.Label);
            }
            //Добавляю вершины
            foreach (var edge in graph.Edges)
            {
                g.AddEdge(new TaggedEdge<string, string>(edge.From, edge.To, edge.Weight.ToString()));
            }
            //Мэджик
            var graphViz = new GraphvizAlgorithm<string, TaggedEdge<string, string>>(g, @".\", GraphvizImageType.Png);
            //Тут формат вершин и ребер
            graphViz.FormatVertex += FormatVertex;
            graphViz.FormatEdge += FormatEdge;
            graphViz.Generate(new FileDotEngine(), "ww.dot");
        }

        private static void FormatVertex(object sender, FormatVertexEventArgs<string> e)
        {
            e.VertexFormatter.Label = e.Vertex;

            e.VertexFormatter.Shape = GraphvizVertexShape.Circle;

            e.VertexFormatter.StrokeColor = GraphvizColor.Black;

            e.VertexFormatter.Font = new GraphvizFont(FontFamily.GenericSansSerif.Name, 14);
        }

        private static void FormatEdge(object sender, FormatEdgeEventArgs<string, TaggedEdge<string, string>> e)
        {
            e.EdgeFormatter.Label.Value = e.Edge.Tag;

            e.EdgeFormatter.Font = new GraphvizFont(FontFamily.GenericSansSerif.Name, 13);
        }
    }

    public class FileDotEngine : IDotEngine
    {
        public string Run(GraphvizImageType imageType, string dot, string outputFileName)
        {
            using (StreamWriter writer = new StreamWriter(outputFileName))
            {
                writer.Write(dot);
            }
            return System.IO.Path.GetFileName(outputFileName);
        }
    }
}
