﻿namespace GraphWinVisualisator
{
    using System;
    using System.Windows.Forms;
    using ImportExportLibrary.Entities;
    using ImportExportLibrary.Import;
    using System.Collections.Generic;
    using System.Linq;
    using System.Diagnostics;
    using System.Drawing;
    using System.IO;
    using System.Threading;

    public partial class MainForm : Form
    {
        public Graph Graph { get; set; }

        private FileDialog _fileDialog = new OpenFileDialog();

        private InvariantsForm _invariantsForm;

        private string _imageName = "ww.png";

        public MainForm()
        {
            InitializeComponent();
            инвариантыToolStripMenuItem.Visible = false;
            btnSearchGraphEdge.Enabled = IsEdgeSearchEnabled();
        }

        private void инвариантыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _invariantsForm = new InvariantsForm(Graph);
            _invariantsForm.ShowDialog();
        }

        private void ImportFromAdjacencyMatrix(bool isOriented = false)
        {
            _fileDialog.Title = "Матрица смежности";
            if (_fileDialog.ShowDialog() == DialogResult.OK)
            {
                Graph = new ImportService(_fileDialog.FileName, isOriented).FromAdjacencyMatrix();
                lblFrom.Text = "матрицы смежности";
                InitGraph();
                инвариантыToolStripMenuItem.Visible = true;
            }
        }

        private void ImportFromIncidenceMatrix(bool isOriented = false)
        {
            _fileDialog.Title = "Матрица инцидентности";
            if (_fileDialog.ShowDialog() == DialogResult.OK)
            {
                Graph = new ImportService(_fileDialog.FileName, isOriented).FromIncidenceMatrix();
                lblFrom.Text = "матрицы инцидентности";
                InitGraph();
                инвариантыToolStripMenuItem.Visible = true;
            }
        }

        private void ImportFromAdjacencyList(bool isOriented = false)
        {
            _fileDialog.Title = "Список смежности";
            if (_fileDialog.ShowDialog() == DialogResult.OK)
            {
                Graph = new ImportService(_fileDialog.FileName, isOriented).FromAdjacencyList();
                lblFrom.Text = "списка смежности";
                InitGraph();
                инвариантыToolStripMenuItem.Visible = true;
            }
        }

        private void ImportFromEdgeList(bool isOriented = false)
        {
            _fileDialog.Title = "Список ребер";
            if (_fileDialog.ShowDialog() == DialogResult.OK)
            {
                Graph = new ImportService(_fileDialog.FileName, isOriented).FromEdgeList();
                lblFrom.Text = "списка ребер";
                InitGraph();
                инвариантыToolStripMenuItem.Visible = true;
            }
        }

        private void toolStripMenuItem4_Click(object sender, EventArgs e)
        {
            ImportFromEdgeList();
        }

        private void InitGraph()
        {
            lblGraphType.Text = Graph.IsOriented ? "Ориентированный" : "Неориентированный";
            InitDGVVertices();
            InitDataSourceToComboboxes(Graph.Vertices);
            InitDgvEdges();
        }

        private void InitDGVVertices()
        {
            dgvVertices.Rows.Clear();
            foreach (var vertex in Graph.Vertices)
            {
                int newRowIndex = dgvVertices.Rows.Add(vertex.Label);
                dgvVertices.Rows[newRowIndex].Tag = vertex;
            }
        }

        private void dgvVertices_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            var vertex = (GraphVertex)e.Row.Tag;
            Graph.DeleteVertex(vertex.Label);
            InitDataSourceToComboboxes(Graph.Vertices);
            InitDgvEdges();
        }

        private void ShowAdjacencyVertices(List<GraphVertex> vertices)
        {
            dgvAdjacencyVertices.Rows.Clear();
            if (vertices != null)
            {
                foreach (var vertex in vertices)
                {
                    int newRowIndex = dgvAdjacencyVertices.Rows.Add(vertex.Label);
                    dgvAdjacencyVertices.Rows[newRowIndex].Tag = vertex;
                }
            }
        }

        private void dgvVertices_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            var vertex = (GraphVertex)dgvVertices.Rows[e.RowIndex].Tag;
            ShowAdjacencyVertices(Graph.FindAdjacentVertices(vertex?.Label));
        }

        private void InitDataSourceToComboboxes(List<GraphVertex> vertices)
        {
            cbFrom.Items.Clear();
            cbTo.Items.Clear();
            foreach (var vertex in vertices)
            {
                cbFrom.Items.Add(vertex.Label);
                cbTo.Items.Add(vertex.Label);
            }
        }

        private void InitDgvEdges()
        {
            dgvEdges.Rows.Clear();
            dgvEdges.Columns.Clear();
            var column1 = new DataGridViewComboBoxColumn()
            {
                HeaderText = "Вершина 1",
                AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
            };
            var column2 = new DataGridViewComboBoxColumn()
            {
                HeaderText = "Вершина 2",
                AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
            };
            foreach (var vertex in Graph.Vertices)
            {
                column1.Items.Add(vertex.Label);
                column2.Items.Add(vertex.Label);
            }
            dgvEdges.Columns.Add(column1);
            dgvEdges.Columns.Add(column2);
            dgvEdges.Columns.Add(
                new DataGridViewTextBoxColumn()
                {
                    HeaderText = "Вес"
                });

            foreach (var edge in Graph.Edges)
            {
                int rowIndex = dgvEdges.Rows.Add(edge.From, edge.To, edge.Weight.ToString());
                dgvEdges.Rows[rowIndex].Tag = edge;
            }
        }

        private void dgvVertices_RowValidating(object sender, DataGridViewCellCancelEventArgs e)
        {
            var row = dgvVertices.Rows[e.RowIndex];
            var vertex = (GraphVertex)row.Tag;
            if (dgvVertices.IsCurrentRowDirty)
            {
                if (string.IsNullOrWhiteSpace(row.Cells[0].Value?.ToString()))
                {
                    row.ErrorText = "Метка не должна быть пустой!";
                    e.Cancel = true;
                    return;
                }
                if (vertex == null)
                {
                    Graph.AddVertex(row.Cells[0].Value.ToString());
                    row.Tag = Graph.FindVertex(row.Cells[0].Value.ToString());
                    InitDataSourceToComboboxes(Graph.Vertices);
                    InitDgvEdges();
                }
                else
                {
                    vertex.Label = row.Cells[0].Value.ToString();
                    InitDataSourceToComboboxes(Graph.Vertices);
                    InitDgvEdges();
                }
            }
        }

        private void dgvEdges_RowValidating(object sender, DataGridViewCellCancelEventArgs e)
        {
            var row = dgvEdges.Rows[e.RowIndex];
            var edge = (GraphEdge) row.Tag;
            if (dgvEdges.IsCurrentRowDirty)
            {
                if (string.IsNullOrWhiteSpace(row.Cells[0].Value?.ToString())
                    || string.IsNullOrWhiteSpace(row.Cells[1].Value?.ToString())
                    || string.IsNullOrWhiteSpace(row.Cells[2].Value?.ToString()))
                {
                    row.ErrorText = "Поля не должны быть пустыми!";
                    e.Cancel = true;
                    return;
                }

                int res;
                bool isNumber = int.TryParse(row.Cells[2].Value.ToString(), out res);
                if (!isNumber)
                {
                    row.ErrorText = "Вес должен быть числом!";
                    e.Cancel = true;
                    return;
                }
                else
                {
                    if (res < 0)
                    {
                        row.ErrorText = "Вес должен быть неотрицательным!";
                        e.Cancel = true;
                        return;
                    }
                }

                if (Graph.FindEdgeWeight(row.Cells[0].Value.ToString(), row.Cells[1].Value.ToString()) != -1)
                {
                    row.ErrorText = "Такое ребро уже существует!";
                    e.Cancel = true;
                    return;
                }

                if (edge == null)
                {
                    Graph.AddEdge(
                        row.Cells[0].Value.ToString(),
                        row.Cells[1].Value.ToString(),
                        int.Parse(row.Cells[2].Value.ToString()));
                    row.Tag = edge;
                    row.ErrorText = "";
                }
            }
        }

        private void dgvEdges_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            var edge = (GraphEdge)e.Row.Tag;
            if (edge != null)
            {
                Graph.DeleteEdge(edge.From, edge.To);
            }
            
        }

        private void btnDraw_Click(object sender, EventArgs e)
        {
            if (pBoxGraph.Image != null)
            {
                pBoxGraph.Image.Dispose();
            }
            
            File.Delete(Path.Combine(Environment.CurrentDirectory, _imageName));

            GraphDrawer.DrawGraph(Graph);
            ProcessStartInfo startInfo = new ProcessStartInfo(Environment.CurrentDirectory + "/bin/dot.exe");
            startInfo.Arguments = $"-Tpng ww.dot -o {_imageName}";
            Process.Start(startInfo);
            Thread.Sleep(3000);
            pBoxGraph.Image = new Bitmap($"{Environment.CurrentDirectory}\\{_imageName}");
            
        }

        private void btnVertexSearch_Click(object sender, EventArgs e)
        {
            dgvVertices.ClearSelection();
            var label = tbVertexSearch.Text;
            if (Graph.FindVertex(label) != null)
            {
                for (int i = 0; i < Graph.VerticesCount; i++)
                {
                    if (dgvVertices.Rows[i].Cells[0].Value.ToString() == label)
                    {
                        dgvVertices.Rows[i].Selected = true;
                        dgvVertices.Focus();
                        dgvVertices.CurrentCell = dgvVertices.Rows[i].Cells[0];
                        return;
                    }
                }
            }
            else
            {
                MessageBox.Show("Не найдено такой вершины!");
            }
        }

        private bool IsEdgeSearchEnabled()
        {
            return cbFrom.Text != "" && cbTo.Text != "";
        }

        private void cbFrom_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnSearchGraphEdge.Enabled = IsEdgeSearchEnabled();
        }

        private void cbTo_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnSearchGraphEdge.Enabled = IsEdgeSearchEnabled();
        }

        private void btnSearchGraphEdge_Click(object sender, EventArgs e)
        {
            var from = cbFrom.Text;
            var to = cbTo.Text;
            dgvEdges.ClearSelection();

            if (Graph.IsEdgeExists(from, to))
            {
                for (int i = 0; i < Graph.Edges.Count; i++)
                {
                    if (dgvEdges.Rows[i].Cells[0].Value.ToString() == from
                        && dgvEdges.Rows[i].Cells[1].Value.ToString() == to)
                    {
                        dgvEdges.Rows[i].Selected = true;
                        dgvEdges.Rows[i].Cells[0].Selected = true;
                        dgvEdges.Focus();
                        dgvEdges.CurrentCell = dgvEdges.Rows[i].Cells[0];
                        return;
                    }
                }
            }
            else
            {
                MessageBox.Show("Такого ребра нет!");
            }
        }

        private void dgvEdges_RowValidated(object sender, DataGridViewCellEventArgs e)
        {
        }

        private void матрицаСмежностиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ImportFromAdjacencyMatrix(true);
        }

        private void матрицаИнцидентностиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ImportFromIncidenceMatrix(true);
        }

        private void списокСмежностиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ImportFromAdjacencyList(true);
        }

        private void списокРеберToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ImportFromEdgeList(true);
        }
    }
}
